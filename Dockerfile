# Dockerfile Apache Spark Standalone
# for testing
FROM ubuntu:14.04

MAINTAINER Dirsan <dirsan.s@outlook.com>

RUN apt-get update && apt-get -y install curl wget unzip python-software-properties telnet openssh-server openssh-client

# Install JAVA
ENV JAVA_HOME /usr/local/jdk1.8.0_131
ENV PATH $PATH:$JAVA_HOME/bin
ADD Files/jdk-8u131-linux-x64.tar.gz /usr/local/
RUN ln -s $JAVA_HOME /usr/local/java

#ARG JAVA_ARCHIVE=http://download.oracle.com/otn-pub/java/jdk/8u131-b11/d54c1d3a095b4ff2b6607d096fa80163/jdk-8u131-linux-x64.tar.gz
#RUN curl -sL --retry 3 --insecure --header "Cookie: oraclelicense=accept-securebackup-cookie;" $JAVA_ARCHIVE | tar -xz -C /usr/local/ && ln -s $JAVA_HOME /usr/local/java

#Install Hadoop
ADD Files/hadoop-2.7.3.tar.gz /usr/local
ENV HADOOP_HOME /usr/local/hadoop-2.7.3
ENV PATH $PATH:$HADOOP_HOME/bin:$HADOOP_HOME/sbin
ENV HADOOP_MAPRED_HOME=$HADOOP_HOME
ENV HADOOP_COMMON_HOME=$HADOOP_HOME
ENV HADOOP_HDFS_HOME=$HADOOP_HOME
ENV YARN_MAPRED_HOME=$HADOOP_HOME
ENV HADOOP_MAPRED_HOME=$HADOOP_HOME/lib/native
ENV HADOOP_CONF_DIR=$HADOOP_HOME/etc/hadoop
ENV YARN_CONF_DIR=$HADOOP_HOME/etc/hadoop
ENV HADOOP_OPTS="-Djava.library.path=$HADOOP_HOME/lib"

#Install Spark
ADD Files/spark-2.1.1-bin-hadoop2.7.tgz /usr/local

ENV SPARK_HOME /usr/local/spark-2.1.1-bin-hadoop2.7
ENV PATH $PATH:$SPARK_HOME/bin:/$SPARK_HOME/sbin

#Install Scala
ADD Files/scala-2.12.2.tgz /usr/local
ENV PATH $PATH:$PATH:/usr/local/scala-2.12.2/bin

#Install Scala Build Tools (sbt)
ADD Files/sbt-0.13.15.tgz /usr/local/
ENV PATH $PATH:$PATH:/usr/local/sbt/bin

#Install HBase
#ADD Files/hbase-1.2.6-bin.tar.gz /usr/local
#ENV PATH $PATH:$PATH:/usr/local/hbase-1.2.6/bin

#Setup spark
RUN cp /usr/local/spark-2.1.1-bin-hadoop2.7/conf/spark-env.sh.template /usr/local/spark-2.1.1-bin-hadoop2.7/conf/spark-env.sh
RUN cp /usr/local/spark-2.1.1-bin-hadoop2.7/conf/log4j.properties.template /usr/local/spark-2.1.1-bin-hadoop2.7/conf/log4j.properties
RUN cp /usr/local/spark-2.1.1-bin-hadoop2.7/conf/slaves.template /usr/local/spark-2.1.1-bin-hadoop2.7/conf/slaves

RUN echo  "export SPARK_MASTER_IP=localhost" >> /usr/local/spark-2.1.1-bin-hadoop2.7/conf/spark-env.sh
RUN echo  "export SPARK_WORKER_CORES=1" >> /usr/local/spark-2.1.1-bin-hadoop2.7/conf/spark-env.sh
RUN echo  "export SPARK_WORKER_MEMORY=800m" >> /usr/local/spark-2.1.1-bin-hadoop2.7/conf/spark-env.sh
RUN echo  "export SPARK_WORKER_INSTANCES=1" >> /usr/local/spark-2.1.1-bin-hadoop2.7/conf/spark-env.sh

RUN mkdir -p /usr/local/spark-2.1.1-bin-hadoop2.7/logs
RUN sed -i -e 's/INFO,\ console/WARN,FILE/g' /usr/local/spark-2.1.1-bin-hadoop2.7/conf/log4j.properties
RUN echo "log4j.appender.FILE=org.apache.log4j.FileAppender" >> /usr/local/spark-2.1.1-bin-hadoop2.7/conf/log4j.properties
RUN echo "log4j.appender.FILE.File=/usr/local/spark-2.1.1-bin-hadoop2.7/logs/SparkOut.log" >> /usr/local/spark-2.1.1-bin-hadoop2.7/conf/log4j.properties
RUN echo "log4j.appender.FILE.layout=org.apache.log4j.PatternLayout" >> /usr/local/spark-2.1.1-bin-hadoop2.7/conf/log4j.properties
RUN echo "log4j.appender.FILE.layout.ConversionPattern=%d{yy/MM/dd HH:mm:ss} %p %c{1}: %m%n" >> /usr/local/spark-2.1.1-bin-hadoop2.7/conf/log4j.properties

#SSH startup and keys
RUN ssh-keygen -t rsa -P "" -f $HOME/.ssh/id_rsa && cat $HOME/.ssh/id_rsa.pub >> $HOME/.ssh/authorized_keys
RUN service ssh start

#Setup Hadoop
#Create directory for HDFS
RUN mkdir -p /dockerhadoop/hdfs/namenode/ && mkdir -p /dockerhadoop/hdfs/datanode/
ADD Files/xml/hadoop-env.sh /usr/local/hadoop-2.7.3/etc/hadoop/
ADD Files/xml/core-site.xml /usr/local/hadoop-2.7.3/etc/hadoop/
ADD Files/xml/hdfs-site.xml /usr/local/hadoop-2.7.3/etc/hadoop/
ADD Files/xml/mapred-site.xml /usr/local/hadoop-2.7.3/etc/hadoop/
ADD Files/xml/yarn-site.xml /usr/local/hadoop-2.7.3/etc/hadoop/


# Add Scripts
ADD Files/Submit.sh /usr/local/bin/
ADD Files/Submit2.py /usr/local/bin/
ADD Files/DistributeSetup.sh /usr/local/bin/

# Expose ports for Apache Spark and ssh
EXPOSE 4040 6066 7077 8080 8081 22
# Expose ports for Hadoop
EXPOSE 50070 8088 8030 9000

# Expose ports for hbase
EXPOSE 60010
WORKDIR $SPARK_HOME


#Setup Volumes
VOLUME ["/jars"]
VOLUME ["/output"]
