# Dockerfile Apache Spark Standalone
# Final Copy

FROM ubuntu:14.04

MAINTAINER Dirsan <dirsan.s@outlook.com>

RUN apt-get update && apt-get -y install curl wget unzip python-software-properties

# Install JAVA
ENV JAVA_HOME /usr/local/jdk1.8.0_131
ENV PATH $PATH:$JAVA_HOME/bin
ARG JAVA_ARCHIVE=http://download.oracle.com/otn-pub/java/jdk/8u131-b11/d54c1d3a095b4ff2b6607d096fa80163/jdk-8u131-linux-x64.tar.gz
RUN curl -sL --retry 3 --insecure --header "Cookie: oraclelicense=accept-securebackup-cookie;" $JAVA_ARCHIVE | tar -xz -C /usr/local/ && ln -s $JAVA_HOME /usr/local/java 


#Install Spark
RUN wget https://d3kbcqa49mib13.cloudfront.net/spark-2.1.1-bin-hadoop2.7.tgz && tar -xvf spark-2.1.1-bin-hadoop2.7.tgz -c /usr/local/
ENV SPARK_HOME /usr/local/spark-2.1.1-bin-hadoop2.7
ENV PATH $PATH:$SPARK_HOME/bin

#Install Scala
RUN wget https://downloads.lightbend.com/scala/2.12.2/scala-2.12.2.tgz && tar -xvf scala-2.12.2.tgz -c /usr/local/
ENV PATH $PATH:$PATH:/usr/local/scala-2.12.2/bin

#Install Scala Build Tools (sbt)
RUN wget https://github.com/sbt/sbt/releases/download/v0.13.15/sbt-0.13.15.tgz &&tar -xvf sbt-0.13.15.tgz -C /usr/local/
ENV PATH $PATH:$PATH:/usr/local/sbt/bin

#Setup spark
RUN cp /usr/local/spark-2.1.1-bin-hadoop2.7/conf/spark-env.sh.template /usr/local/spark-2.1.1-bin-hadoop2.7/conf/spark-env.sh
RUN cp /usr/local/spark-2.1.1-bin-hadoop2.7/conf/log4j.properties.template /usr/local/spark-2.1.1-bin-hadoop2.7/conf/log4j.properties
RUN cp /usr/local/spark-2.1.1-bin-hadoop2.7/conf/slaves.template /usr/local/spark-2.1.1-bin-hadoop2.7/conf/slaves

RUN echo  "export SPARK_MASTER_IP=localhost" >> /usr/local/spark-2.1.1-bin-hadoop2.7/conf/spark-env.sh
RUN echo  "export SPARK_WORKER_CORES=1" >> /usr/local/spark-2.1.1-bin-hadoop2.7/conf/spark-env.sh
RUN echo  "export SPARK_WORKER_MEMORY=800m" >> /usr/local/spark-2.1.1-bin-hadoop2.7/conf/spark-env.sh
RUN echo  "export SPARK_WORKER_INSTANCES=1" >> /usr/local/spark-2.1.1-bin-hadoop2.7/conf/spark-env.sh

RUN mkdir -p /usr/local/spark-2.1.1-bin-hadoop2.7/logs
RUN sed -i -e 's/INFO,\ console/WARN,FILE/g' /usr/local/spark-2.1.1-bin-hadoop2.7/conf/log4j.properties
RUN echo "log4j.appender.FILE=org.apache.log4j.FileAppender" >> /usr/local/spark-2.1.1-bin-hadoop2.7/conf/log4j.properties
RUN echo "log4j.appender.FILE.File=/usr/local/spark-2.1.1-bin-hadoop2.7/logs/SparkOut.log" >> /usr/local/spark-2.1.1-bin-hadoop2.7/conf/log4j.properties
RUN echo "log4j.appender.FILE.layout=org.apache.log4j.PatternLayout" >> /usr/local/spark-2.1.1-bin-hadoop2.7/conf/log4j.properties
RUN echo "log4j.appender.FILE.layout.ConversionPattern=%d{yy/MM/dd HH:mm:ss} %p %c{1}: %m%n" >> /usr/local/spark-2.1.1-bin-hadoop2.7/conf/log4j.properties

# Add Scripts
ADD Files/Submit.sh /usr/local/bin/
ADD Files/Submit2.py /usr/local/bin/

# Expose ports for Apache Spark
EXPOSE 4040 6066 7077 8080
WORKDIR $SPARK_HOME


#Setup Volumes
VOLUME ["/jars"]
VOLUME ["/output"]
